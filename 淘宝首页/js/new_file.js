//搜索导航栏
$(".search-nav li").on("click",function(){
	$(".search-nav .active").removeClass("active");
	$(this).addClass("active");
})

//关闭二维码
$(".header .ma div").on("click",function(){
	$(".header .ma").hide()
})

//二级菜单
var jsonArr=[]
$(".firl").on('mouseenter',function(){
	
	$(this).find("a").each(function(index,ele){
	  	$(".content-body .hovl h3 a")[index].innerText=ele.innerText;
	})
	
	$(this).index()
	  
	$(".top-left .hov").show()
}).on('mouseleave',function(){
	$(".top-left .hov").hide()
})

$(".top-left .hov").on('mouseenter',function(){
	$(".top-left .hov").show()
}).on('mouseleave',function(){
	$(".top-left .hov").hide()
})

//轮播图
var a=$(".lunbo1").sliderBox({
	imgArr:[
		"img/sliderimg/sliderimg1.jpg",
		"img/sliderimg/sliderimg2.jpg",
		"img/sliderimg/sliderimg3.jpg"
	]
})
var b=$(".lunbo2").sliderBox({
	imgArr:[
		"img/sliderimg/sliderimg3.jpg",
		"img/sliderimg/sliderimg2.jpg",
		"img/sliderimg/sliderimg1.jpg"
	]
})


function init(){
	var moveHeight=75,
		len=$(".ttlunbo li").length,
		index=0,
		timer;
	function bindEvent(){
		$(".ttlunbo").hover(function(){
			clearInterval(timer);
		},function(){
			slideAuto()
		})
		
		window.blur=function(){
			clearInterval(timer);
		}
		window.focus=function(){
			slideAuto()
		}
	}
	function move(){
		index++;
		if(index==len){
			index=0;
		}
		$(".ttlunbo ul").animate({"top":-index*moveHeight+"px"},200,"swing");
	}
	function slideAuto(){
		timer=setInterval(function(){
			move();
		},3000)
	}	
	bindEvent();
	slideAuto();
}
init();

//搜索框二维码
$(".floor2 .left .title>i").hover(function(){
	$(".floor2 .ma").show()
},function(){
	$(".floor2 .ma").hide()
})

//个人中心服务
$(".shop").on("mouseenter",function(){
	$(this).addClass("active");
	$(".shop").not(this).removeClass("active");
	$(".fuwu").show()
	$(".fuwu").on("click",function(){
		this.flag=true;
	})
})

$(".fourbox li a").on("mouseenter",function(){
	$(this).addClass("red");
	$(".fourbox li a").not(this).removeClass("red");
})

$(".x").click(function(){
	$(".fuwu").hide()
	$(".shop").removeClass("active");
})

$(document).click(function(e){
	if(!$(".fuwu")[0].flag){
		$(".fuwu").hide()
		$(".shop").removeClass("active");
	}
	$(".fuwu")[0].flag=false;
})

if (innerWidth<1190) {
	suoxiao()
} else{
	fangda()
}

//缩小视窗
window.onresize=function(){
	if (innerWidth<1190) {
		suoxiao()
	} else{
		fangda()
	}
}

function suoxiao(){
	$(".search").css("margin","0 145px 0 218px");
	$(".headerbox .ma").css("right","10px");
	
	$(".top-right").hide()
	$(".content-head ul:last-child").hide()
	
	$(".nav .wrapper,.header .wrapper,.content .wrapper").width(1020)
	$(".content-body .left").width(700)
	$(".content-body .bottom").width(720)
	$(".ttlunbo").width(480)
	$(".content-body .bottom .more").hide()
	$(".fubiao").css("margin-right","-570px");
	
}

function fangda(){
	$(".search").css("margin","0 258px 0 280px");
	$(".headerbox .ma").css("right","120px");
	$(".top-right").show()
	$(".content-head ul:last-child").show()
	
	$(".wrapper").width(1190)
	$(".content-body .left").width(900)
	$(".content-body .bottom").width(890)
	$(".ttlunbo").width(676)
	$(".content-body .bottom .more").show()
	$(".fubiao").css("margin-right","-655px");
}

//倒计时
var time1=new Date();
time1.setDate(time1.getDay()+1);
setInterval(function(){
	var time=time1-new Date();
	time=new Date(time);
	var arr=[time.getHours(),time.getMinutes(),time.getSeconds()].map(function(e){
		return ("0"+e).slice(-2);
	})
	$(".hours").text(arr[0]);
	$(".minutes").text(arr[1]);
	$(".seconds").text(arr[2]);
},200)

//浮标
document.onscroll=function(){
	var y=$(document).scrollTop();
	if(y>135){
		$(".header").addClass("wfixed")
	}else{
		$(".header").removeClass("wfixed")
	}
	
	if(y>403){
		$(".fubiao").addClass("fixed")
	}else{
		$(".fubiao").removeClass("fixed")
	}
	
	if(y>955){
		$(".fubiao a").eq(5).css("display","inline-block")
	}else{
		$(".fubiao a").eq(5).hide()
	}
	
	if(y<1970){
		changStyle(0);
	}else if(y<2820){
		changStyle(1);
	}else if(y<3600){
		changStyle(2);
	}else if(y<4850){
		changStyle(3);
	}else{
		changStyle(4);
	}
}

setTimeout(function(){
	$(".fubiao").fadeIn()
},400)

$(".fubiao a").on("click",function(){
	var index=$(this).index();
	$(document).scrollTop()
	switch(index){
		case 1:
			scrollmove(965);
			break;
		case 2:
			scrollmove(1970);
			break;
		case 3:
			scrollmove(2820);
			break;
		case 4:
			scrollmove(3600);
			break;
		case 5:
			scrollmove(4850);
			break;
		case 6:
			scrollmove(0);
			break;	
	}
})
function scrollmove(target){
	$("html,body").stop().animate({"scrollTop":target},500,"swing")
}

function changStyle(index){
	$(".fubiao .active").removeClass("active")
	$(".fubiao a").eq(index).addClass("active");
}