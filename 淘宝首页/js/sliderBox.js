!function(){
	$.fn.extend({
		sliderBox:function(options){
			options.father=this||$("body");
			new swiper(options);
		}
	})
	
	function swiper(options){
		this.opts=options||{};
		this.init()
	}
	
	swiper.prototype={
		init:function(){
			var opts=this.opts;
			this.wrap=opts.father;
			this.imgSrc=opts.imgArr;
			this.time=opts.time||4000;
			this.len=opts.imgArr.length;
			this.moveWidth=this.wrap.width();
			this.nowIndex=0;
			this.flag=true;
			
			this.createDom();
			this.bindEvent();
			this.slideAuto();
		},
		createDom:function(){
			var wrap=this.wrap,
				imgArr=this.imgSrc,
				imgBox='<ul class="imgBox">',
				btn='<div class="leftBtn btn">&lt;</div>\
					 <div class="rightBtn btn">&gt;</div>\
					 <div class="indexBox"><ul>';
			for (var i=0,l=this.len;i<l;i++) {
				imgBox+='<li><img src='+imgArr[i]+'></li>';
				btn+="<li>";
			}
			imgBox+='<li><img src='+imgArr[0]+'></li>';
			$(wrap).append($(imgBox)).append($(btn));
			
			$(".imgBox",$(wrap)).width((this.len+1)*100+"%");
			$(".indexBox li:first").addClass("active");
		},
		bindEvent:function(){
			var self=this,
				wrap=self.wrap;
			$(".indexBox li,.btn",$(wrap)).click(function(){
				if ($(this).hasClass("leftBtn")) {
					self.nowIndex--;
				}else if($(this).hasClass("rightBtn")){
					self.nowIndex++;
				}else{
					self.nowIndex=$(this).index();
				}
				self.move();
			})
			
			
			this.wrap.hover(function(){
				clearTimeout(self.timer);
				$(".btn",$(wrap)).show()
			},function(){
				self.slideAuto()
				$(".btn",$(wrap)).hide()
			}).on("selectstart",function(){
				return false;
			})
		},
		move:function(dir){
			var self=this,
				len=this.len,
				wrap=self.wrap,
				moveWidth=this.moveWidth;
				if(self.nowIndex<0){
					self.nowIndex=len-1;
					$(".imgBox",$(wrap)).css("left",-self.len*self.moveWidth+"px");
				}else if(self.nowIndex>len){
					self.nowIndex=1;
					$(".imgBox",$(wrap)).css("left","0px");
				}
				self.changeStyle();
				$(".imgBox",$(wrap)).stop().animate({"left":-self.nowIndex*self.moveWidth+"px"},250,"swing",function(){
					$(".btn",$(wrap)).css("display")=="none"?self.slideAuto():"";
				});
		},
		changeStyle:function(){
			var self=this;
			var styleIndex=this.nowIndex;
			if(styleIndex==this.len){
				styleIndex=0;
			}
			$(".active",$(self.wrap)).removeClass();
			$(".indexBox li",$(self.wrap)).eq(styleIndex).addClass("active");
		},
		slideAuto:function(){
			var self=this;
			this.timer=setTimeout(function(){
				self.nowIndex++;
				self.move();
			},self.time)
		}
	}
}()